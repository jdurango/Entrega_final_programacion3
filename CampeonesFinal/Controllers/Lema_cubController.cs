﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CampeonesFinal.Models;

namespace CampeonesFinal.Controllers
{
    public class Lema_cubController : Controller
    {
        private ERCampeonesFinalContainer db = new ERCampeonesFinalContainer();

        // GET: Lema_cub
        public ActionResult Index_lema()
        {
            return View(db.Lema_cubSet.ToList());
        }

        // GET: Lema_cub/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lema_cub lema_cub = db.Lema_cubSet.Find(id);
            if (lema_cub == null)
            {
                return HttpNotFound();
            }
            return View(lema_cub);
        }

        // GET: Lema_cub/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Lema_cub/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,nombre,lema")] Lema_cub lema_cub)
        {
            if (ModelState.IsValid)
            {
                db.Lema_cubSet.Add(lema_cub);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(lema_cub);
        }

        // GET: Lema_cub/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lema_cub lema_cub = db.Lema_cubSet.Find(id);
            if (lema_cub == null)
            {
                return HttpNotFound();
            }
            return View(lema_cub);
        }

        // POST: Lema_cub/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nombre,lema")] Lema_cub lema_cub)
        {
            if (ModelState.IsValid)
            {
                db.Entry(lema_cub).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(lema_cub);
        }

        // GET: Lema_cub/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Lema_cub lema_cub = db.Lema_cubSet.Find(id);
            if (lema_cub == null)
            {
                return HttpNotFound();
            }
            return View(lema_cub);
        }

        // POST: Lema_cub/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Lema_cub lema_cub = db.Lema_cubSet.Find(id);
            db.Lema_cubSet.Remove(lema_cub);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

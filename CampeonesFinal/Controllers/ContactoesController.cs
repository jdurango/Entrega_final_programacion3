﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CampeonesFinal.Models;
using System.IO;
using System.Net.Mail;

namespace CampeonesFinal.Controllers
{
    public class ContactoesController : Controller
    {
        private ERCampeonesFinalContainer db = new ERCampeonesFinalContainer();

        // GET: Contactoes
        public ActionResult Index()
        {
            return View(db.ContactoSet.ToList());
        }

        public string enviarCorreo(Contacto contacto)
        {
            string body = string.Empty;
            string resul = null;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Views/Clubs/Contacto.cshtml")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{Nombre}", contacto.nombre);
            body = body.Replace("{Email}", contacto.email);
            body = body.Replace("{Asunto}", contacto.asunto);
            body = body.Replace("{Mensaje}", contacto.mensaje);

            System.Net.Mail.MailMessage _mail = new System.Net.Mail.MailMessage();
            _mail.To.Add("paulischica_@hotmail.com");
            _mail.To.Add("jairo.durango94@hotmail.com");
            _mail.From = new MailAddress("paulischica_@hotmail.com");
            _mail.Subject = "Gracias por su aporte";
            _mail.SubjectEncoding = System.Text.Encoding.UTF8;

            _mail.Body = "";
            _mail.BodyEncoding = System.Text.Encoding.UTF8;
            _mail.Priority = MailPriority.Normal;
            _mail.IsBodyHtml = true;
            //Esta parte es la que cambia con respecto a un servidor SMTP/POP3
            SmtpClient _smtp = new SmtpClient();

            _smtp.Port = 587;
            _smtp.Host = "smtp.outlook.com";
            _smtp.UseDefaultCredentials = false;
            _smtp.Credentials = new System.Net.NetworkCredential("jairo.durango94@hotmail.com", "XXXXXXXX");
            _smtp.EnableSsl = true;
            //Esto es para que vaya a través de SSL que es obligatorio con GMail  
            try
            {
                _smtp.Send(_mail);
                resul = "El e-mail ha sido enviado con éxito. Muy pronto atenderemos su requerimiento. Gracias";
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                resul = "Error:" + ex.Message;
            }

            return resul;
        }
        // GET: Contactoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contacto contacto = db.ContactoSet.Find(id);
            if (contacto == null)
            {
                return HttpNotFound();
            }
            return View(contacto);
        }

        // GET: Contactoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contactoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,nombre,email,asunto,mensaje")] Contacto contacto)
        {
            if (ModelState.IsValid)
            {
                db.ContactoSet.Add(contacto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contacto);
        }

        // GET: Contactoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contacto contacto = db.ContactoSet.Find(id);
            if (contacto == null)
            {
                return HttpNotFound();
            }
            return View(contacto);
        }

        // POST: Contactoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nombre,email,asunto,mensaje")] Contacto contacto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contacto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contacto);
        }

        // GET: Contactoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contacto contacto = db.ContactoSet.Find(id);
            if (contacto == null)
            {
                return HttpNotFound();
            }
            return View(contacto);
        }

        // POST: Contactoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contacto contacto = db.ContactoSet.Find(id);
            db.ContactoSet.Remove(contacto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

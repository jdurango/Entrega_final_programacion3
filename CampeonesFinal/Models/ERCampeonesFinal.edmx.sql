
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/01/2015 03:19:44
-- Generated from EDMX file: c:\users\hp\documents\visual studio 2013\Projects\CampeonesFinal\CampeonesFinal\Models\ERCampeonesFinal.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BDCampeonesFinal];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ClubSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClubSet];
GO
IF OBJECT_ID(N'[dbo].[Lema_cubSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Lema_cubSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ClubSet'
CREATE TABLE [dbo].[ClubSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [mision] nvarchar(max)  NOT NULL,
    [vision] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Lema_cubSet'
CREATE TABLE [dbo].[Lema_cubSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [lema] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ContactoSet'
CREATE TABLE [dbo].[ContactoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [nombre] nvarchar(max)  NOT NULL,
    [email] nvarchar(max)  NOT NULL,
    [asunto] nvarchar(max)  NOT NULL,
    [mensaje] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ClubSet'
ALTER TABLE [dbo].[ClubSet]
ADD CONSTRAINT [PK_ClubSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Lema_cubSet'
ALTER TABLE [dbo].[Lema_cubSet]
ADD CONSTRAINT [PK_Lema_cubSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ContactoSet'
ALTER TABLE [dbo].[ContactoSet]
ADD CONSTRAINT [PK_ContactoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------